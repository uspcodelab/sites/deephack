export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'DeepHack',
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "Ajude o TCE-SP a identificar os objetivos de desenvolvimento sustentavel para 2030 no estado de SP"
      },
      {
        property: "og:title",
        content: "DeepHack"
      },
      {
        property: "og:description",
        content:
          "Ajude o TCE-SP a identificar os objetivos de desenvolvimento sustentavel para 2030 no estado de SP"
      },
      {
        property: "og:image",
        content:
          "https://firebasestorage.googleapis.com/v0/b/uclsanca.appspot.com/o/deephack.jpg?alt=media&token=6a205e71-4ce9-4ade-b84b-3bf3720ccb25"
      },
      {
        property: "og:image:type",
        content: "image/jpeg"
      },
      {
        property: "og:image:width",
        content: "800"
      },
      {
        property: "og:image:height",
        content: "600"
      },
      { name: 'apple-mobile-web-app-title', content: 'DeepHack' },
      { name: 'application-name', content: 'DeepHack' },
      { name: 'msapplication-TileColor', content: '#101010' },
      { name: 'theme-color', content: '#101010' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicon/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon/favicon-16x16.png' },
      { rel: 'mask-icon', href: '/favicon/safari-pinned-tab.svg', color: '#101010' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "plugins/vue-smooth-scroll.js", ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
